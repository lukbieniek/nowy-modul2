<?php
  add_filter("acf/settings/load_json", "sw_nowy-modul2_acf_json_load_point");
  function sw_nowy-modul2_acf_json_load_point( $paths ) {
    $paths[] = dirname(__FILE__) . "/acf-fields";
    return $paths;
  }
  
  foreach (glob( __DIR__ . "/functions/*.php") as $filename) {
    include $filename;
  }
  